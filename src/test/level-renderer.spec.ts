import canvas from "canvas";
import chai from "chai";
import fs from "fs";
import { fromSysPath } from "furi";
import sysPath from "path";
import pixelmatch from "pixelmatch";
import url from "url";
import { AlphaCell, Level } from "../lib/core/level";
import { loadImage, ServerBitmapService } from "../lib/server/bitmap";
import { LevelRendererService } from "../lib/server/level-renderer";
import { InMemoryBackgroundService } from "../lib/universal/background";
import meta from "./meta.js";

const BITMAP_SERVICE: ServerBitmapService = new ServerBitmapService();
BITMAP_SERVICE.register({
  id: "00000000-0000-0000-0000-000000000000",
  width: 128,
  height: 128,
  uri: fromSysPath(sysPath.join(meta.dirname, "cailloux4.png")),
});

const BACKGROUND_SERVICE: InMemoryBackgroundService = new InMemoryBackgroundService([
  {id: "00000000-0000-0000-0000-000000000000", bitmapId: "00000000-0000-0000-0000-000000000000"},
]);

const LEVEL_RENDERER_SERVICE: LevelRendererService = new LevelRendererService(BACKGROUND_SERVICE, BITMAP_SERVICE);

describe("levelRender", function () {
  it("empty", async function () {
    const level: Level = {
      id: "00000000-0000-0000-0000-000000000000",
      width: 20,
      height: 25,
      cellWidth: 20,
      cellHeight: 20,
      borderLeftWidth: 10,
      borderRightWidth: 10,
      alphaLayer: [AlphaCell.Void, AlphaCell.Tile],
      backgroundId: undefined,
    };

    const actualCanvas: canvas.Canvas = await LEVEL_RENDERER_SERVICE.getCanvas(level).toPromise();
    const actualPngBuffer: Buffer = await toPngBuffer(actualCanvas);
    await writeFile(fromSysPath(sysPath.join(meta.dirname, "empty.out.png")), actualPngBuffer);
    const expectedUri: url.URL = fromSysPath(sysPath.join(meta.dirname, "empty.png"));
    const expectedCanvas: canvas.Image = await loadImage(expectedUri).toPromise();
    const comparison: ImageComparison = await compareImages(actualCanvas, expectedCanvas);
    if (comparison.sameSize) {
      const diffPngBuffer: Buffer = await toPngBuffer(comparison.diffImage);
      await writeFile(fromSysPath(sysPath.join(meta.dirname, "empty.diff.png")), diffPngBuffer);
    }
    assertSimilarImages(comparison);
  });

  it("rocks", async function () {
    const level: Level = {
      id: "00000000-0000-0000-0000-000000000000",
      width: 20,
      height: 25,
      cellWidth: 20,
      cellHeight: 20,
      borderLeftWidth: 10,
      borderRightWidth: 10,
      alphaLayer: [AlphaCell.Void, AlphaCell.Tile],
      backgroundId: "00000000-0000-0000-0000-000000000000",
    };

    const actualCanvas: canvas.Canvas = await LEVEL_RENDERER_SERVICE.getCanvas(level).toPromise();
    const actualPngBuffer: Buffer = await toPngBuffer(actualCanvas);
    await writeFile(fromSysPath(sysPath.join(meta.dirname, "rocks.out.png")), actualPngBuffer);
    const expectedUri: url.URL = fromSysPath(sysPath.join(meta.dirname, "rocks.png"));
    const expectedCanvas: canvas.Image = await loadImage(expectedUri).toPromise();
    const comparison: ImageComparison = await compareImages(actualCanvas, expectedCanvas);
    if (comparison.sameSize) {
      const diffPngBuffer: Buffer = await toPngBuffer(comparison.diffImage);
      await writeFile(fromSysPath(sysPath.join(meta.dirname, "rocks.diff.png")), diffPngBuffer);
    }
    assertSimilarImages(comparison);
  });
});

async function toPngBuffer(cvs: canvas.Canvas): Promise<Buffer> {
  return new Promise<Buffer>((resolve, reject) => {
    cvs.toBuffer(
      (err: Error | null, buffer: Buffer): void => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(buffer);
        }
      },
      "image/png",
    );
  });
}

type ImageLike = canvas.Canvas | canvas.Image;

async function asImageData(input: ImageLike): Promise<canvas.ImageData> {
  const ctx: canvas.CanvasRenderingContext2D = canvas.createCanvas(input.width, input.height).getContext("2d");
  ctx.drawImage(input, 0, 0);
  return ctx.getImageData(0, 0, input.width, input.height);
}

type ImageComparison = ImageComparisonDifferentSize | ImageComparisonSameSize;

interface ImageComparisonDifferentSize {
  actual: ImageLike;
  expected: ImageLike;
  sameSize: false;
}

interface ImageComparisonSameSize {
  actual: ImageLike;
  expected: ImageLike;
  sameSize: true;
  diffCount: number;
  diffImage: canvas.Canvas;
}

async function compareImages(actual: ImageLike, expected: ImageLike): Promise<ImageComparison> {
  if (actual.width !== expected.width || actual.height !== expected.height) {
    return {actual, expected, sameSize: false};
  }
  const actualImageData: canvas.ImageData = await asImageData(actual);
  const expectedImageData: canvas.ImageData = await asImageData(expected);
  const diffImage: canvas.Canvas = canvas.createCanvas(expected.width, expected.height);
  const diffCtx: canvas.CanvasRenderingContext2D = diffImage.getContext("2d");
  const diffData: canvas.ImageData = diffCtx.getImageData(0, 0, expected.width, expected.height);
  const diffCount: number = pixelmatch(
    actualImageData.data as any as Uint8Array,
    expectedImageData.data as any as Uint8Array,
    diffData.data as any as Uint8Array,
    expected.width,
    expected.height,
    {threshold: 0.05},
  );
  diffCtx.putImageData(diffData, 0, 0);
  // console.warn(diffCount);
  return {actual, expected, sameSize: true, diffCount, diffImage};
}

function assertSimilarImages(comparison: ImageComparison): void | never {
  if (!comparison.sameSize) {
    throw new chai.AssertionError("Images do not have the same size");
  }
  const pixelCount: number = comparison.expected.width * comparison.expected.height;
  const ratio: number = comparison.diffCount / pixelCount;
  const THRESHOLD: number = 0.0001;
  if (ratio > THRESHOLD) {
    throw new chai.AssertionError(
      `Image difference above threshold: ${comparison.diffCount} / ${pixelCount} = ${ratio} > ${THRESHOLD}`,
    );
  }
}

async function writeFile(filePath: fs.PathLike, data: Buffer): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
