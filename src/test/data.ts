import fs from "fs";
import path from "path";
import meta from "./meta.js";

const BIDIRECTIONAL_PATH: string = path.resolve(meta.dirname, "bidirectional.json");
const EMIT_ONLY_PATH: string = path.resolve(meta.dirname, "emit-only.json");
const PARSE_ONLY_PATH: string = path.resolve(meta.dirname, "parse-only.json");

export interface TestItem {
  js: string;
  mtbon: string;
}

export function getEmitTestItems(): TestItem[] {
  return [...getBidirectionalTestItems(), ...getEmitOnlyTestItems()];
}

export function getParseTestItems(): TestItem[] {
  return [...getBidirectionalTestItems(), ...getParseOnlyTestItems()];
}

function getBidirectionalTestItems(): TestItem[] {
  const bidirectional: TestItem[] = readJsonSync(BIDIRECTIONAL_PATH);
  const large: ReadonlyArray<string> = ["lvl10"];
  for (const item of large) {
    const resolvedJson: string = path.resolve(meta.dirname, `${item}.json`);
    const resolvedMtbon: string = path.resolve(meta.dirname, `${item}.mtbon`);
    const json: any = readJsonSync(resolvedJson);
    const mtbon: string = readMtbonSync(resolvedMtbon);
    const js: string = `(${JSON.stringify(json)})`;
    bidirectional.push({js, mtbon});
  }
  return bidirectional;
}

function getEmitOnlyTestItems(): TestItem[] {
  return readJsonSync(EMIT_ONLY_PATH);
}

function getParseOnlyTestItems(): TestItem[] {
  return readJsonSync(PARSE_ONLY_PATH);
}

function readJsonSync(p: string): any {
  const buffer: Buffer = fs.readFileSync(p);
  return JSON.parse(buffer.toString("UTF-8"));
}

function readMtbonSync(p: string): string {
  const buffer: Buffer = fs.readFileSync(p);
  return buffer.toString("UTF-8").trim();
}
