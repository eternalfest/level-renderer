import * as rx from "rxjs";
import { Background, BackgroundService } from "../core/background";

export class InMemoryBackgroundService implements BackgroundService {
  private readonly idToBackground: ReadonlyMap<string, Background>;

  constructor(backgrounds: Iterable<Background>) {
    const idToBackground: Map<string, Background> = new Map();
    for (const background of backgrounds) {
      idToBackground.set(background.id, background);
    }
    this.idToBackground = idToBackground;
  }

  getById(backgroundId: string): rx.Observable<Background> {
    const background: Background | undefined = this.idToBackground.get(backgroundId);
    if (background === undefined) {
      return rx.throwError(new Error(`BackgroundNotFound: ${backgroundId}`));
    } else {
      return rx.of(background);
    }
  }
}
