// import * as rx from "rxjs";
// import * as rxOp from "rxjs/operators";
// import { BitmapMeta, BitmapService } from "../core/bitmap";
//
// export interface BrowserBitmapMeta extends BitmapMeta {
//   uri: URL;
// }
//
// export class BrowserBitmapService implements BitmapService<HTMLImageElement> {
//   private readonly bitmapIdToMeta: Map<string, BrowserBitmapMeta>;
//
//   constructor() {
//     this.bitmapIdToMeta = new Map();
//   }
//
//   register(meta: BrowserBitmapMeta): void {
//     this.bitmapIdToMeta.set(meta.id, meta);
//   }
//
//   loadById(bitmapId: string): rx.Observable<HTMLImageElement> {
//     const meta: BrowserBitmapMeta | undefined = this.bitmapIdToMeta.get(bitmapId);
//     if (meta === undefined) {
//       return rx.throwError(new Error(`BitmapNotFound: ${bitmapId}`));
//     } else {
//       return loadImage(meta.uri);
//     }
//   }
// }
//
// export function loadImage(uri: URL): rx.Observable<HTMLImageElement> {
//   return new rx.Observable((subscriber: rx.Subscriber<HTMLImageElement>): rx.TeardownLogic => {
//     const img: HTMLImageElement = new Image();
//     img.addEventListener("error", onError);
//     img.addEventListener("load", onLoad);
//     img.src = uri.toString();
//     return teardown;
//
//     function onLoad(/* ev: Event */) {
//       teardown();
//       subscriber.next(img);
//       subscriber.complete();
//     }
//
//     function onError(ev: ErrorEvent): void {
//       teardown();
//       subscriber.error(new Error(ev.message));
//     }
//
//     function teardown() {
//       img.removeEventListener("load", onLoad);
//       img.removeEventListener("error", onError);
//     }
//   }).pipe(rxOp.shareReplay(1));
// }
