import canvas from "canvas";
import * as rx from "rxjs";
import * as rxOp from "rxjs/operators";
import { Background, BackgroundService } from "../core/background";
import { Level } from "../core/level";
import { Renderer2D } from "../core/renderer";
import { ServerBitmapService } from "./bitmap";

export class LevelRendererService {
  private readonly background: BackgroundService;
  private readonly bitmap: ServerBitmapService;

  constructor(background: BackgroundService, bitmap: ServerBitmapService) {
    this.background = background;
    this.bitmap = bitmap;
  }

  getBgRenderer(level: Level): rx.Observable<Renderer2D> {
    if (level.backgroundId === undefined) {
      return rx.of((ctx: CanvasRenderingContext2D) => renderDefaultBg(ctx, level));
    }

    return this.background.getById(level.backgroundId).pipe(
      rxOp.flatMap((bg: Background) => this.bitmap.getById(bg.bitmapId)),
      rxOp.map((bitmap: canvas.Image): Renderer2D => {
        const cvs: canvas.Canvas = canvas.createCanvas(420, 500);
        const ctx: canvas.CanvasRenderingContext2D = cvs.getContext("2d")!;
        const pattern: CanvasPattern | null = ctx.createPattern(bitmap as any as CanvasImageSource, "repeat");

        if (pattern === null) {
          throw new Error("CannotCreatePattern");
        }

        return (ctx: CanvasRenderingContext2D): void => {
          ctx.translate(level.borderLeftWidth, 0);
          ctx.fillStyle = pattern;
          ctx.fillRect(0, 0, level.cellWidth * level.width, level.cellHeight * level.height);
        };
      }),
    );
  }

  getCanvas(level: Level): rx.Observable<canvas.Canvas> {
    return this.getBgRenderer(level).pipe(
      rxOp.map((renderer: Renderer2D): canvas.Canvas => {
        const cvs: canvas.Canvas = canvas.createCanvas(420, 500);
        const ctx: canvas.CanvasRenderingContext2D = cvs.getContext("2d");
        renderer(ctx);
        return cvs;
      }),
      rxOp.shareReplay(1),
    );
  }
}

export function renderDefaultBg(ctx: CanvasRenderingContext2D, level: Level) {
  ctx.save();
  try {
    ctx.translate(level.borderLeftWidth, 0);
    ctx.scale(level.cellWidth, level.cellHeight);
    for (let x: number = 0; x < level.width; x++) {
      for (let y: number = 0; y < level.height; y++) {
        const opacity: number = (1 + ((x + y) % 2)) / 10;
        ctx.fillStyle = `rgba(63, 63, 63, ${opacity.toString(10)})`;
        ctx.fillRect(x, y, 1, 1);
      }
    }
  } finally {
    ctx.restore();
  }
}

// export function rxLevelImageData(level: Level): rx.Observable<canvas.ImageData> {
//   return rxLevelBgRenderer(level).pipe(
//     rxOp.map((renderer: Renderer2D): canvas.ImageData => {
//       const cvs: canvas.Canvas = canvas.createCanvas(420, 500);
//       const ctx: canvas.CanvasRenderingContext2D = cvs.getContext("2d");
//       renderer(ctx);
//       return ctx.getImageData(0, 0, 420, 500);
//     }),
//     rxOp.shareReplay(1),
//   );
// }
