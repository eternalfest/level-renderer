import canvas from "canvas";
import { toSysPath } from "furi";
import * as rx from "rxjs";
import * as rxOp from "rxjs/operators";
import url from "url";
import { BitmapMeta, BitmapService } from "../core/bitmap";

export interface ServerBitmapMeta extends BitmapMeta {
  uri: url.URL;
}

export class ServerBitmapService implements BitmapService<canvas.Image> {
  private readonly bitmapIdToMeta: Map<string, ServerBitmapMeta>;

  constructor() {
    this.bitmapIdToMeta = new Map();
  }

  register(meta: ServerBitmapMeta): void {
    this.bitmapIdToMeta.set(meta.id, meta);
  }

  getById(bitmapId: string): rx.Observable<canvas.Image> {
    const meta: ServerBitmapMeta | undefined = this.bitmapIdToMeta.get(bitmapId);
    if (meta === undefined) {
      return rx.throwError(new Error(`BitmapNotFound: ${bitmapId}`));
    } else {
      return loadImage(meta.uri);
    }
  }
}

export function loadImage(uri: url.URL): rx.Observable<canvas.Image> {
  return new rx.Observable((subscriber: rx.Subscriber<canvas.Image>): rx.TeardownLogic => {
    const img: canvas.Image = new canvas.Image();

    // TODO: Do not restore the old values, simply set to `undefined`.
    const oldOnError: typeof img.onerror = img.onerror;
    const oldOnLoad: typeof img.onload = img.onload;

    img.onerror = onError;
    img.onload = onLoad;
    img.src = toSysPath(uri.toString());
    return teardown;

    function onLoad() {
      teardown();
      subscriber.next(img);
      subscriber.complete();
    }

    function onError(err: Error): void {
      teardown();
      subscriber.error(err);
    }

    function teardown() {
      img.onerror = oldOnError;
      img.onload = oldOnLoad;
    }
  }).pipe(rxOp.shareReplay(1));
}
