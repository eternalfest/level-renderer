import * as rx from "rxjs";

export interface BitmapMeta {
  readonly id: string;
  readonly width: number;
  readonly height: number;
}

export interface BitmapService<TImage> {
  getById(bitmapId: string): rx.Observable<TImage>;
}
