import * as rx from "rxjs";

export interface Background {
  readonly id: string;
  readonly bitmapId: string;
}

export interface BackgroundService {
  getById(backgroundId: string): rx.Observable<Background>;
}
