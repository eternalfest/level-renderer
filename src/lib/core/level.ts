export interface Level {
  readonly id: string;
  readonly width: 20;
  readonly height: 25;
  readonly cellWidth: 20;
  readonly cellHeight: 20;
  readonly borderLeftWidth: 10;
  readonly borderRightWidth: 10;
  readonly alphaLayer: ReadonlyArray<AlphaCell>;
  readonly backgroundId?: string;
}

export enum AlphaCell {
  Void,
  Tile,
}

// export type BackgroundSkin = StaticBackgroundSkin;
//
// export interface StaticBackgroundSkin {
//   readonly id: string;
//   readonly type: "static";
//   readonly textureId: string;
// }
