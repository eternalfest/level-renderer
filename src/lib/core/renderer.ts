export type Renderer2D = (ctx: CanvasRenderingContext2D) => void;
